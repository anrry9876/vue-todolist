import Vue from 'vue'
import Router from 'vue-router'
import Home from '../components/HelloWorld.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/todoList',
      name: 'todoList',
      component: () => import('../pages/todoList.vue')
    },
    {
      path: '/eventbus',
      name: 'eventbus',
      component: () => import('../pages/testEventBus.vue')
    }
  ]
})
